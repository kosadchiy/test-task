<?php

function readTheFile($path) {
    $handle = fopen($path, "r");

    while(!feof($handle)) {
        yield trim(fgets($handle));
    }

    fclose($handle);
}

function printResult($result) {
    foreach ($result as $key => $value) { 
        echo "$key - $value\n";
    }
}

$result = [];
$iterator = readTheFile($argv[1]);

foreach ($iterator as $iteration) {
    preg_match_all("/[0-9]+/", $iteration, $matches);
    for ($i = 0; $i < count($matches[0]); $i++) {
        if (isset($result[$matches[0][$i]])) {
            $result[$matches[0][$i]]++;
        } else {
            $result[$matches[0][$i]] = 1;
        }
    }
}
arsort($result);
printResult($result);

require "memory.php";